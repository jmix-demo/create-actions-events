package kz.vainshtein.create_action_events.screen.sampleentity;

import io.jmix.ui.Notifications;
import io.jmix.ui.screen.*;
import kz.vainshtein.create_action_events.entity.SampleEntity;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("SampleEntitySimple.browse")
@UiDescriptor("sample-entity-simple-browse.xml")
@LookupComponent("sampleEntitiesTable")
public class SampleEntitySimpleBrowse extends StandardLookup<SampleEntity> {

    @Autowired private Notifications notifications;

    @Install(to = "sampleEntitiesTable.create", subject = "afterCloseHandler")
    private void sampleEntitiesTableCreateAfterCloseHandler(AfterCloseEvent afterCloseEvent) {
        notifications.create()
                     .withCaption("Created")
                     .show();
    }


}