package kz.vainshtein.create_action_events.screen.sampleentity;

import io.jmix.ui.screen.*;
import kz.vainshtein.create_action_events.entity.SampleEntity;

@UiController("SampleEntity.edit")
@UiDescriptor("sample-entity-edit.xml")
@EditedEntityContainer("sampleEntityDc")
public class SampleEntityEdit extends StandardEditor<SampleEntity> {
}