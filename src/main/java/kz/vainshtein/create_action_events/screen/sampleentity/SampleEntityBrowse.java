package kz.vainshtein.create_action_events.screen.sampleentity;

import io.jmix.ui.screen.LookupComponent;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;
import kz.vainshtein.create_action_events.screen.abstractentity.AbstractEntityBrowse;

@UiController("SampleEntity.browse")
@UiDescriptor("sample-entity-browse.xml")
@LookupComponent("entitiesTable")
public class SampleEntityBrowse extends AbstractEntityBrowse {
}