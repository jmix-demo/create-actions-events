package kz.vainshtein.create_action_events.screen.sampleentity;

import io.jmix.ui.screen.*;
import kz.vainshtein.create_action_events.entity.SampleEntity;

@UiController("SampleEntitySimple.edit")
@UiDescriptor("sample-entity-simple-edit.xml")
@EditedEntityContainer("sampleEntityDc")
public class SampleEntitySimpleEdit extends StandardEditor<SampleEntity> {
}