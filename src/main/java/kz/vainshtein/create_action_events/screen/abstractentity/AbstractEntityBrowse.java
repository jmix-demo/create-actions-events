package kz.vainshtein.create_action_events.screen.abstractentity;

import io.jmix.ui.Notifications;
import io.jmix.ui.screen.*;
import kz.vainshtein.create_action_events.entity.AbstractEntity;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("AbstractEntity.browse")
@UiDescriptor("abstract-entity-browse.xml")
@LookupComponent("entitiesTable")
public abstract class AbstractEntityBrowse extends StandardLookup<AbstractEntity> {

    @Autowired private Notifications notifications;

    @Install(to = "entitiesTable.create", subject = "afterCloseHandler")
    private void entitiesTableCreateAfterCloseHandler(AfterCloseEvent ignored) {
        notifications.create()
                     .withCaption("Created")
                     .show();
    }
}