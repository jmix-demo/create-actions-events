package kz.vainshtein.create_action_events.screen.abstractentity;

import io.jmix.ui.screen.*;
import kz.vainshtein.create_action_events.entity.SampleEntity;

@UiController("AbstractEntity.edit")
@UiDescriptor("abstract-entity-edit.xml")
@EditedEntityContainer("entityDc")
public class AbstractEntityEdit extends StandardEditor<SampleEntity> {
}